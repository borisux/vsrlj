#!/bin/bash

# vsrlj -- VSR license jockey. Vsrlj script automatically installs VSR license to specified qcow2 image file.
# How to use:
# ./vsrlj.sh - l <path to licese file> -p <path to qcow2 image>

# Basically script performs two steps:
# 1) license file will be copied to disk image
# 2) BOF file located in disk image will be modified to point to the license file copied in step 1.
# 3) If the -f flag is given, script will populate nvram.dat file with the contents of the nvram string for 14.0.R5 released
# this is done to overcome the issue with the need of 4GB ram for VSRs 14.0.6+. See README.md for details

echo "
                 _ _
                | (_)
 __   _____ _ __| |_
 \ \ / / __| '__| | |
  \ V /\__ \ |  | | |
   \_/ |___/_|  |_| |
                 _/ |
                |__/
"
echo -e "\nvsrlj -- VSR license jockey: Adding VSR license automatically
(c) Roman Dodin\n\n"

fix_nvram=false

while getopts ":l:p:f" opt; do
  case $opt in
    l) lic_file="$OPTARG"
    ;;
    p) image_path="$OPTARG"
    ;;
    f) fix_nvram=true
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

cat > bof.cfg <<EOF
primary-image    cf3:\timos\i386-both.tim
primary-config   cf3:\config.cfg
license-file     cf3:\{{lic_file}}
autonegotiate
duplex           full
speed            100
wait             3
persist          off
no li-local-save
no li-separate
no fips-140-2
console-speed    115200
EOF

sed -i "s/{{lic_file}}/$(basename $lic_file)/g" bof.cfg

lic_file_full_path=`readlink -f $lic_file`
bof_full_path=`readlink -f bof.cfg`

echo "Copying license file - $lic_file - to the disk image..."
echo "copy-in $lic_file_full_path /
copy-in $bof_full_path /" > guestfish_cmds

if [ "$fix_nvram" = true ];
then
echo "Fixing nvram.dat"
cat > nvram.dat <<EOF
ata=0,2(0,0)0.0.0.0:cf3:/timos/i386-boot.tim tn=vRR f=0x8 s=pl=1 o=sr12#niente_a_r2_6f,lava_r3|23|0
EOF
nvram_full_path=`readlink -f nvram.dat`
echo "copy-in $nvram_full_path /" > guestfish_cmds
fi

guestfish --rw -a $image_path -m /dev/sda1 < guestfish_cmds

echo "Deleting temporary files..."
rm -f guestfish_cmds bof.cfg

if [ "$fix_nvram" = true ];
then
rm -f nvram.dat
echo "nvram.dat fixed!"
fi

echo "License copied!"
