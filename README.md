# About
vsrlj -- VSR license jockey. This `bash` script provisions VSR license to a qcow2 disk image automatically.

Read [this post](http://noshut.ru/2016/09/using-guestfish-to-modify-vm-disk-image/) for full details on this approach

# Problem statement
Every time there is a new VSR released one need to add appropriate license blob to it.
This procedure is painful as it requires >3 manual operations.

Moreover, thanks to Kivanc Imer who discovered that Since 14.R6, the nvram.dat is changed to this: `ata=0,2(0,0)0.0.0.0:cf3:/timos/i386-boot.tim tn=VSR-I f=0x8 s=pl=1 o=vsr-c#vsr_cpm,vsr_iom|158|0|0|0`. And it needs 4Gb minimum while 2G ram was enough before. Therefore he added an additional parameter `-f` to change nvram to the previous 2Gb hungry state.


# What does this script do?
vsrlj takes a license provided by user and a path to the disk image. It then performs these three steps:
1. Opens disk image
2. Adds license file to the disk image
3. Modifies `bof.cfg` file to point to the license file location
4. If the `-f` flag  is given, nvram.dat file will be populated with the following content `ata=0,2(0,0)0.0.0.0:cf3:/timos/i386-boot.tim tn=vRR f=0x8 s=pl=1 o=sr12#niente_a_r2_6f,lava_r3|22|0`

Thus you will have appropriate license applied and/or nvram changed in an automatic way.

# How to use it?
As this script relies on guestfish utility you first need to install it:
- for Debian-based systems: `sudo apt-get install libguestfs-tools`
- for RHEL/CentOS-based systems: `sudo yum install libguestfs`

You have to have a license file and a disk image accessible for this script. For example, you can store lic.txt file in the scripts directory and run `vsrlg.sh` as follows:
```
# tree -L 1
.
├── lic.txt
└── vsrlj.sh
```

`./vsrlj.sh -l lic.txt -p /opt/unetlab/addons/qemu/timos14-14.0.R4/hda_test.qcow2`

where:
- `-l <path to license file>`
- `-p path to qcow2 VSR disk image`
- `-f` to fix nvram.dat file


Also note, that `lic.txt` file should contain license blob.
